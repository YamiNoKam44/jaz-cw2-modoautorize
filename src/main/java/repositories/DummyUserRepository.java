package repositories;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import domain.User;

public class DummyUserRepository implements UserRepository{

	private static List<User> db = new ArrayList<User>();
	
	private void println(String string) {
		}
	
	@Override
	public User emailChecker(String email) {
		for(User user: db){
			println("Konto z takim e-mailem aktualnie istnieje!!!");
		}
		return null;
	}

	@Override
	public User loginChecker(String login) {
		for(User user: db){
			println("Ten u�ytkownik ju� istnieje!!!");
		}
		return null;
	}
	
	@Override
	public void createNewUser(User user) {
		db.add(user);
		
	}
}
