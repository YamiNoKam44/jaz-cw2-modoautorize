package repositories;

import domain.User;

public interface UserRepository {
	User emailChecker(String email);   
	User loginChecker(String login);	 
	void createNewUser(User user);
	;
	
}
