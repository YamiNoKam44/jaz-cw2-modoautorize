<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML> 
<html lang='pl'> 
<head>
	<meta charset='utf-8'>
	<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1'>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<title>Premium Manager</title>
</head>
<body>
<div class='container'>

	<h1>Wpisz użytkownika by przyznać/usunąć konto premium</h1>
	<form action="premiumMan" method="post">
		<label>Username:<input type="text" id="login" name="login"/></label><br>
		<label><input type="radio" name="permissions" value="premium" checked/>Dodaj konto premium</label><br/>
		<label><input type="radio" name="permissions" value="normal"/>Usuń konto premium</label><br/>
		<input type="submit" value="Change account"/>
	</form>
</div>
</body>
</html>